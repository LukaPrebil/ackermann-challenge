#include <string>
#include <iostream>
#include "boost/multiprecision/cpp_int.hpp"

using bint = boost::multiprecision::cpp_int;

bint int_pow(bint base, bint exponent){
    bint result(1);
    while (exponent){
        if (exponent & 1){
            result *= base;
        }
        exponent >>= 1;
        base *= base;
    }
    return result;
}

bint ackermann(unsigned m, unsigned n){
    static bint (*ack)(unsigned, bint) = [](unsigned m, bint n)->bint{
        switch (m){
            case 0:
                return n+1;
            case 1:
                return n+2;
            case 2:
                return 3 + 2*n;
            case 3:
                return 5 + 8 * (int_pow(bint(2), n) - 1);
            default:
                if (n==0){
                    return ack(m-1, bint(1));
                }else{
                    return ack(m-1, ack(m, n-1));
                }
        }
    };
    return ack(m, bint(n));
}

int main(int argc, char* argv[]){
    if(argc < 3){
        std::cerr << "needs CLI arguments for M and N\n";
        return 1;
    }
    unsigned m = atoi(argv[1]);
    unsigned n = atoi(argv[2]);

    std::cout << "m = " << m << ", n = " << n << std::endl;
    std::cout << "Ackermann(" << m << ", " << n << ") = " << ackermann(m, n) << std::endl;
    return 0;
}